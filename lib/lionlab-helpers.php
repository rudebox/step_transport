<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

	 
	/**
	* Load an inline SVG.
	*
	* @param string $filename The filename of the SVG you want to load.
	*
	* @return string The content of the SVG you want to load.
	*/
	function load_inline_svg( $filename ) {
	 
	    // Add the path to your SVG directory inside your theme.
	    $svg_path = '/images/svgs/';
	 
	    // Check the SVG file exists
	    if ( file_exists( get_stylesheet_directory() . $svg_path . $filename ) ) {
	 
	        // Load and return the contents of the file
	        return file_get_contents( get_stylesheet_directory_uri() . $svg_path . $filename );
	    }
	 
	    // Return a blank string if we can't find the file.
	    return '';
	}


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_titel', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	//get current template
	function get_current_template() {
  		global $template;
  		return basename($template, '.php');
	}

	//get current page ID
	function get_current_page_id() {
    	global $wp_query; 
    	$page = $wp_query->get_queried_object(); 
    	$page_id = $page->ID;
    	return $page_id = $page->ID;
	}

?>