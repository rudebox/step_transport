<?php 
/**
* Description: Lionlab keywords banner global repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

if (have_rows('keywords', 'options') ) :

?>

<section class="keyword-banner pink--bg">
	<div class="wrap--fluid">

		<div class="flex flex--wrap">
			<marquee class="keyword-banner__marquee" behavior="" direction="">
				<?php while (have_rows('keywords', 'options') ) : the_row(); 
					$keyword = get_sub_field('keyword');
				?>
				
					<span class="keyword-banner__keyword"><?php echo esc_html($keyword); ?><i>-</i></span>

				<?php endwhile; ?>
			</marquee>
		</div>

	</div>
</section>
<?php endif; ?>