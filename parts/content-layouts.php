<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'references' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'references' ); ?>

    <?php
    } elseif( get_row_layout() === 'quick-links' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'quick-links' ); ?>

    <?php
    } elseif( get_row_layout() === 'instagram' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'instagram' ); ?>

    <?php
    } elseif( get_row_layout() === 'reviews' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'review' ); ?> 

    <?php
    } elseif( get_row_layout() === 'testimonials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'testimonials' ); ?> 

    <?php
    } elseif( get_row_layout() === 'feed' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'feed' ); ?> 

    <?php
    } elseif( get_row_layout() === 'jobs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'jobs' ); ?>

    <?php
    } elseif( get_row_layout() === 'gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'usp' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'usp' ); ?>

    <?php
    } elseif( get_row_layout() === 'cta' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cta' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
