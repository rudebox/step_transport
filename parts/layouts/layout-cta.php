<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_sub_field('cta_bg');
$title = get_sub_field('cta_text');
$link = get_sub_field('cta_link');

?>

<section class="cta--map b-lazy" data-src=<?php echo esc_url($img['url']); ?>);>
	<div class="wrap hpad">
		<div class="cta__item--map flex flex--justify flex--center">
			<h2 class="cta__title--map center"><?php echo esc_html($title); ?></h2>
			<a class="btn btn--white cta__btn--map" href="<?php echo esc_url($link); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-round-forward.svg'); ?></span></a>
		</div>		
	</div>
</section>