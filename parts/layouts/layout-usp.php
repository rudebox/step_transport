<?php 
/**
* Description: Lionlab usp repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta_title');

if (have_rows('usp') ) :
?>

<section class="usp <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>		
		<h2 class="usp__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<?php if ($meta_title) : ?>		
		<h6 class="usp__meta-title meta-title"><?php echo $meta_title; ?></h6>
		<?php endif; ?>

		<div class="col-sm-12 usp__item pink--bg flex flex--wrap">

			<?php while (have_rows('usp') ) : the_row(); 				
				$text = get_sub_field('text');							
			?>

			<div class="usp__usp anim fade-in"><?php echo esc_html($text); ?></div>

			<?php endwhile; ?>
		</div>
		
	</div>
</section>
<?php endif; ?>