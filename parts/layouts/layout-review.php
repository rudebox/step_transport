<?php 
/**
* Description: Lionlab instagram gallery repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

$title = get_sub_field('review_title');
$score = get_sub_field('review_score');
$total = get_sub_field('review_total');
$link = get_sub_field('review_link');
$link_text = get_sub_field('review_link_text');

?>

<section class="review padding--<?php echo esc_attr($margin); ?>" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="wrap hpad">
	
		<?php if ($title) : ?>
			<h2 class="review__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<div class="row">

			<div class="col-sm-6 review__item anim fade-up">
			<h4 class="review__title" itemprop="ratingValue">Bedømmelse: <?php echo $score; ?></h4>
			</div>

			<div class="col-sm-6 review__item anim fade-up">
				<h4 class="review__title" itemprop="reviewCount">Baseret på: <span><?php echo $total; ?></span> bedømmelser</h4>
			</div>

			<?php if (have_rows('review') ) : while(have_rows('review') ) : the_row(); 
				$title = get_sub_field('title');
				$punctuality = get_sub_field('punctuality');
				$quality = get_sub_field('quality');
				$communication = get_sub_field('communication');
				$total = get_sub_field('total');

				//star rating - this is pretty redundant i guess - but shit works
				
				//punctuality
				if ($punctuality === '1') {
					$star = 'review__score--1';
				} elseif ($punctuality === '2') {
					$star = 'review__score--2';
				} elseif ($punctuality === '3') {
					$star = 'review__score--3';
				} elseif ($punctuality === '4') {
					$star = 'review__score--4';
				} elseif ($punctuality === '5') {
					$star = 'review__score--5';
				}

				//quality
				if ($quality === '1') {
					$star_q = 'review__score--1';
				} elseif ($quality === '2') {
					$star_q = 'review__score--2';
				} elseif ($quality === '3') {
					$star_q = 'review__score--3';
				} elseif ($quality === '4') {
					$star_q = 'review__score--4';
				} elseif ($quality === '5') {
					$star_q = 'review__score--5';
				}

				//communication
				if ($communication === '1') {
					$star_c = 'review__score--1';
				} elseif ($communication === '2') {
					$star_c = 'review__score--2';
				} elseif ($communication === '3') {
					$star_c = 'review__score--3';
				} elseif ($communication === '4') {
					$star_c = 'review__score--4';
				} elseif ($communication === '5') {
					$star_c = 'review__score--5';
				}

				//total
				if ($total === '1') {
					$star_t = 'review__score--1';
				} elseif ($total === '2') {
					$star_t = 'review__score--2';
				} elseif ($total === '3') {
					$star_t = 'review__score--3';
				} elseif ($total === '4') {
					$star_t = 'review__score--4';
				} elseif ($total === '5') {
					$star_t = 'review__score--5';
				}

			?>

			<div class="col-sm-12 review__list anim fade-up">
				<h5 class="review__list--title" itemprop="name"><?php echo esc_html($title); ?></h5>
				<div class="row">
					<div class="col-sm-3" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">Punktlighed<br> <span class="review__score <?php echo esc_attr($star); ?>"></span></div>
					<div class="col-sm-3" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">Arbejdskvalitet<br> <span class="review__score <?php echo esc_attr($star_q); ?>"></span></div>
					<div class="col-sm-3" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">Kommunikation<br> <span class="review__score <?php echo esc_attr($star_c); ?>"></span></div>
					<div class="col-sm-3" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">Samlet bedømmelse<br> <span class="review__score <?php echo esc_attr($star_t); ?>"></span> </div>
				</div>
			</div>

			<?php endwhile; endif; ?>

		</div>

		<?php if ($link) : ?>
			<a class="btn btn--hollow anim fade-up instagram__btn" target="_blank" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?>
			</a>
		<?php endif; ?>

	</div>
</section>