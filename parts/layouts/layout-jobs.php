<?php 
/**
* Description: Lionlab jobs repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

//fallback
$empty = get_sub_field('job_empty');

?>

<section class="jobs padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<?php if (have_rows('job') ) : ?>

			<div class="row">
				<ul class="jobs__table-header col-sm-12">
					<div class="row flex flex--wrap">
						<li class="jobs__table-item col-sm-4">
							<?php _e('Stilling', 'lionlab'); ?>
						</li>

						<li class="col-sm-4 jobs__table-item">
							<?php _e('Stillingstype', 'lionlab'); ?>
						</li>

						<li class="col-sm-4 jobs__table-item">
							<?php _e('Arbejdssted', 'lionlab'); ?>
						</li>
					</div>
				</ul>
			</div>

			<div class="row">
				<?php  while (have_rows('job') ) : the_row(); 
					$title = get_sub_field('job_title');
					$desc = get_sub_field('job_description');
					$location = get_sub_field('job_location');
					$type = get_sub_field('job_type');
				?>
				
				<ul class="jobs__table-list col-sm-12" itemscope itemtype="http://schema.org/JobPosting">
					<div class="row flex flex--wrap">
						
						<div class="jobs__table-trigger">
							<li class="col-sm-4 jobs__table-item jobs__table-item--title" itemprop="title">
								<?php echo esc_html($title); ?>	
							</li>
	 
							<li class="col-sm-4 jobs__table-item jobs__table-item--type" itemprop="workHours">
								<?php echo esc_html($type); ?>	
							</li>

							<li class="col-sm-4 jobs__table-item jobs__table-item--location" itemprop="addressLocality">
								<?php echo esc_html($location); ?>	
							</li>
						</div>

						<li class="jobs__table-item--description col-sm-12" itemprop="description">
							<?php echo $desc; ?>
						</li>

					</div>

				</ul>

				<?php endwhile; ?>

				<?php else:  ?>

				<?php 
					// if no jobs available
					if ($empty) : 
					
						echo '<div class="row">';
					 		echo '<div class="col-sm-12">' . $empty . '</div>'; 
						echo '</div>';

			 		endif; 
			 	?>

		 	<?php endif; ?>

		</div>
	</div>
</section>
