<?php 
/**
* Description: Lionlab social feed repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

$title = get_sub_field('title');
$meta_title = get_sub_field('meta_title');
$icon = get_sub_field('icon');
$link = get_sub_field('link');
$link_text = get_sub_field('link_text');

if (have_rows('embeds') ) :

?>

<section class="embed padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		
		<?php if ($icon) : ?>
			<div class="embed__title-wrap">
				<?php echo $icon; ?>
		<?php endif; ?>

		<?php if ($title) : ?>
			<h2 class="embed__title"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<?php if ($meta_title) : ?>
			<h6 class="embed__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
		<?php endif; ?>

		<?php if ($icon) : ?>
			</div>
		<?php endif; ?>

		<div class="row flex flex--wrap">
			<?php while (have_rows('embeds') ) : the_row(); 
				$embed = get_sub_field('embed');
			?>

			<div class="col-sm-4 embed__item anim fade-up">
				<div class="iframe-container">
				<?php if ($embed) : ?>
					<?php echo $embed; ?>
				<?php endif; ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>

		<?php if ($link) : ?>
			<a class="btn btn--hollow anim fade-up embed__btn" target="_blank" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?>
			</a>
		<?php endif; ?>

	</div>
</section>
<?php endif; ?>