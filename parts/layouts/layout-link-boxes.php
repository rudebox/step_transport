<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :

?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row link-boxes__row">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
				$icon = get_sub_field('icon');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 link-boxes__item anim fade-up b-lazy" data-src="<?php echo $icon['url']; ?>">

				<div class="link-boxes__wrap center">
					<h3 class="link-boxes__title h5"><?php echo esc_html($title); ?></h3>
					<span class="btn btn--pink link-boxes__btn"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-round-forward.svg'); ?></span></span> 
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>