<?php 
/**
* Description: Lionlab quick-links repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$intro_text = get_sub_field('intro_text');

if (have_rows('quick_links') ) :
?>

<section class="quick-links <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($intro_text) : ?>		
		<h2 class="quick-links__header quick-links__header"><?php echo $title; ?></h2>
		<div class="quick-links__intro">
			<?php echo $intro_text; ?>
		</div>
		<?php else : ?>
		<h2 class="quick-links__header h1"><?php echo $title; ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('quick_links') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('img');
				$link = get_sub_field('link');

			?>

			<a href="<?php echo esc_url($link); ?>" class="quick-links__link col-sm-6">
				<div class="quick-links__item quick-links__item--img center anim fade-up">
					<?php if ($img) : ?>
					<img class="b-lazy"  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo esc_url($img['sizes']['quick-links']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
					<?php endif; ?>
				</div>

				<div class="quick-links__item quick-links__item--text anim fade-up">
					<h4 class="quick-links__title"><?php echo esc_html($title); ?></h4>
					<?php if ($text) : ?>
					<p><?php echo $text; ?></p>
					<?php endif; ?>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>