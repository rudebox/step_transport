<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

//gallery
$gallery = get_sub_field('gallery');
$bg = get_sub_field('gallery_img');

//counter
$i=0;

if ( $gallery ) : ?>

	<section class="gallery padding--<?php echo esc_attr($margin); ?>" itemscope itemtype="http://schema.org/ImageGallery">
		<div class="wrap hpad">

			<div class="row gallery__row"> 
				
				
				<div class="gallery__bg--wrap">
					<div class="gallery__bg">
						<img src="<?php echo esc_url($bg['sizes']['logo']); ?>" alt="<?php echo esc_attr($bg['alt']); ?>">
					</div>
				</div>
				
				<?php if ($bg) : ?>
				<div class="gallery__mason gallery__mason--top">
				<?php else : ?>
				<div class="gallery__mason">
				<?php endif; ?>

					<?php
						// Loop through gallery
						foreach ( $gallery as $image ) :

						$i++;

						//wrap every second element	
						if ($i %2 == 1) {

							echo '<div class="gallery__wrap flex flex--wrap">';

						}
					?>

						<figure class="gallery__item gallery__item--<?php echo esc_attr($i); ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
							<a href="<?= $image['sizes']['large']; ?>" class="js-zoom gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
								<img class="gallery__image" data-src="<?= $image['url']; ?>" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
							</a>
						</figure>
						<?php if ($i %2 == 0) { echo '</div>'; } ?> 
					<?php endforeach; ?>
				</div>			
			</div>
		</div>
	</section>

<?php endif; ?>