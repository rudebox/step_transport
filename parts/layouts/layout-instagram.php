<?php 
/**
* Description: Lionlab instagram gallery repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

$title = get_sub_field('title');
$meta_title = get_sub_field('meta_title');
$icon = get_sub_field('icon');
$link = get_sub_field('link');
$link_text = get_sub_field('link_text');

if (have_rows('instagram_img') ) :

?>

<section class="instagram padding--<?php echo esc_attr($margin); ?>" itemscope itemtype="http://schema.org/ImageGallery">
	<div class="wrap hpad">
		
		<?php if ($icon) : ?>
			<div class="instagram__title-wrap">
				<?php echo $icon; ?>
		<?php endif; ?>

		<?php if ($title) : ?>
			<h2 class="instagram__title"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<?php if ($meta_title) : ?>
			<h6 class="instagram__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
		<?php endif; ?>

		<?php if ($icon) : ?>
			</div>
		<?php endif; ?>

		<div class="row flex flex--wrap">
			<?php while (have_rows('instagram_img') ) : the_row(); 
				$img = get_sub_field('img');
			?>

			<div class="col-sm-3 instagram__item anim fade-up" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
				<?php if ($img) : ?>
				<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo esc_url($img['sizes']['social']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>

		<?php if ($link) : ?>
			<a class="btn btn--hollow anim fade-up instagram__btn" target="_blank" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?>
			</a>
		<?php endif; ?>

	</div>
</section>
<?php endif; ?>