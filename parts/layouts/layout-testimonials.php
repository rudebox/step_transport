<?php
/**
 * Description: Lionlab testimonials repeater field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$header = get_sub_field('testimonials_header');
$sub_header = get_sub_field('testimonials_meta_title');

if ( have_rows('testimonial') ) : 
?>

  <section class="testimonials">
    <div class="wrap hpad">
      
      <div class="testimonials__header-wrap">
        <h2 class="testimonials__header center"><?php echo esc_html($header); ?></h2>
        <h6 class="testimonials__meta-title meta-title center"><?php echo esc_html($sub_header); ?></h6>
        <?php echo file_get_contents('wp-content/themes/step_transport/assets/img/quote.svg'); ?>
      </div>

          <div class="slider__track is-slider">

            <?php
            // Loop through slides
            while ( have_rows('testimonial') ) :
              the_row();
              $img   = get_sub_field('img');
              $text = get_sub_field('slides_text');
              $name = get_sub_field('name'); 
              $company_name = get_sub_field('company_name'); 
            ?>
              
              <?php if ($img) : ?>  
              <div class="testimonials__item testimonials__item--offset pink--bg col-sm-10 col-sm-offset-1">
              <?php else : ?> 
              <div class="testimonials__item pink--bg col-sm-10 col-sm-offset-1">
              <?php endif; ?>
                  <?php if ($img) : ?>       
                  <img class="testimonials__img" src="<?php echo esc_url($img['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
                  <?php endif; ?>
                  <div class="testimonials__text">
                    <em><?php echo $text; ?></em>
                    <strong class="testimonials__name"><?php echo esc_html($name); ?></strong>
                    <strong class="testimonials__name"><?php echo esc_html($company_name); ?></strong>
                  </div>
              </div>

            <?php endwhile; ?>

          </div>

      </div>
  
  </section>
<?php endif; ?>