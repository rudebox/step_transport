<?php 
	//video src
	$video_file = get_sub_field('video_file');

	//video teaser
	$teaser = get_sub_field('video_teaser');

	//poster
	$poster = get_sub_field('video_poster');

	//content
	$text = get_sub_field('video_text');
	$title = get_sub_field('video_title');
?>

<section class="video">
	<div class="wrap--fluid flex flex--hvalign">

		<video class="video__video video__video--teaser" autoplay preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ" style="background-image: url(<?php echo esc_url($poster['url']); ?>)">
			<source src="<?php echo esc_url($teaser); ?>" type="video/mp4" codecs="avc1, mp4a">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>	
	
		<video class="video__video video__video--full" preload="auto" muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ" style="background-image: url(<?php echo esc_url($poster['url']); ?>)">
			<source src="<?php echo esc_url($video_file); ?>" type="video/mp4" codecs="avc1, mp4a">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>	
		
		<div class="wrap hpad video__container flex">
			<div class="row">

				<div class="video__text">
					<h2 class="h1 video__title"><?php echo esc_html($title); ?></h2>
					<?php echo esc_html($text); ?>
				</div>

				<a class="video__controls no-ajax flex flex--wrap flex--hvalign" data-fancybox href="<?php echo esc_url($video_file); ?>">
					<div class="video__controls--inner">
		    			<i class="icon ion-ios-play btn btn--video"></i>
					</div>
				</a>
				
			</div>
		</div>

	</div>
</section>