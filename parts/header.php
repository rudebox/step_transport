<!doctype html>

<html <?php language_attributes(); ?> class="js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WV8MH5W" 
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php if (is_front_page() ) : ?>
<header class="header header--front" id="header" itemscope itemtype="http://schema.org/WPHeader">
<?php else : ?>
<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
<?php endif; ?>
  <div class="wrap--fluid hpad flex flex--center flex--justify">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
     ?>
    
    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
       <?php echo $logo_markup; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <?php if (is_front_page() ) : ?>
    <nav class="nav nav--front" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <?php else : ?>
    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <?php endif; ?>

      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
        <?php scratch_contact_nav(); ?>
      </div>
    </nav>

  </div>
</header>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 
