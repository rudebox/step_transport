<?php 
	//video src
	$video_file = get_field('video_file');

	//video teaser
	$teaser = get_field('video_teaser');

	//poster
	$poster = get_field('video_poster');

	//content
	$text = get_field('video_text');
	$meta_title = get_field('video_meta_title');
	$title = get_field('video_title');
	$link = get_field('video_link');
	$link_2 = get_field('video_link_2');
	$link_text = get_field('video_link_text');

	//video start
	$start = get_field('video_start');
?>

<section class="video">
	<div class="video__wrap wrap--fluid flex flex--hvalign" style="background-image: url(<?php echo esc_url($poster['url']); ?>)">

		<video id="js-video-teaser" class="video__video video__video--teaser" autoplay playsinline preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
			<?php if ($start) :  ?>
			<source src="<?php echo esc_url($video_file); ?>#t=<?php echo esc_attr($start); ?>" type="video/mp4" codecs="avc1, mp4a">
			<?php else: ?>
			<source src="<?php echo esc_url($video_file); ?>" type="video/mp4" codecs="avc1, mp4a">
			<?php endif; ?>
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>
	
		<video class="video__video video__video--full" preload="auto" muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
			<source src="<?php echo esc_url($video_file); ?>" type="video/mp4" codecs="avc1, mp4a">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>	
		
		<div class="wrap hpad video__container">
			<div class="row flex flex--wrap video__row">

				<div class="video__text">
					<h6 class="video__meta-title"><?php echo $meta_title; ?></h6>
					<h2 class="h1 video__title"><?php echo $title; ?></h2>
					<?php echo esc_html($text); ?>

					<div class="video__controls--wrap flex flex--wrap flex--valign">
						<a class="btn btn--hollow video__btn" target="_blank" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
						<a class="btn btn--hollow btn--hollow-active video__btn" href="<?php echo esc_url($link_2); ?>"><?php _e('Kontakt', 'lionlab'); ?></a>
					</div>

				</div>							
				
			</div>
		</div>

	</div>
</section>