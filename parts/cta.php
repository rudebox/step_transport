<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_field('cta_bg', 'options');
$title = get_field('cta_title', 'options');
$link = get_field('cta_link', 'options');

//media type
$link = get_field('cta_link', 'options');
?>

<section class="cta b-lazy" data-src="<?php echo esc_url($img['url']); ?>">
	<div class="wrap hpad">
		<div class="row">
			<div class="cta__item center">
				<h2 class="cta__title center"><?php echo esc_html($title); ?></h2>
				<a class="btn btn--white cta__btn" href="<?php echo esc_url($link); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-round-forward.svg'); ?></span></a>
			</div>
		</div>
	</div>
</section>