<?php 
/*
* Template Name: Contact
*/

//contact fields
$text = get_field('contact_text');
$title = get_field('contact_title');
$img = get_field('contact_img');

//social fields
$fb = get_field('facebook', 'options');
$ig = get_field('instagram', 'options');
$lk = get_field('Linkedin', 'options');
$tw = get_field('twitter', 'options');

get_template_part('parts/header'); the_post(); ?> 

<main>
	<?php get_template_part('parts/page', 'header');?>

	<section class="contact padding--bottom">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 contact__text" itemscope itemtype="http://schema.org/LocalBusiness">
					<h2 class="contact__title"><?php echo $title; ?></h2>
					<?php echo $text; ?>
					
					<?php if ($fb) : ?>
					<a target="_blank" class="btn btn--gray contact__social no-ajax" href="<?php echo esc_url($fb); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/fb.svg'); ?></span></a>
					<?php endif; ?>
					
					<?php if ($ig) : ?>
					<a target="_blank" class="btn btn--gray contact__social no-ajax" href="<?php echo esc_url($ig); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/ig.svg'); ?></span></a>
					<?php endif; ?>

					<?php if ($lk) : ?>
					<a target="_blank" class="btn btn--gray contact__social no-ajax" href="<?php echo esc_url($lk); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/lk.svg'); ?></span></a>
					<?php endif; ?>

					<?php if ($tw) : ?>
					<a target="_blank" class="btn btn--gray contact__social no-ajax" href="<?php echo esc_url($tw); ?>"><span><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/arrow-circle.svg'); ?><?php echo file_get_contents('wp-content/themes/step_transport/assets/img/tw.svg'); ?></span></a>
					<?php endif; ?>

				</div>

				<div class="col-sm-6 contact__img">
					<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo esc_url($img['sizes']['large']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				</div>

				<div class="contact__form--controls center col-sm-12 padding--top">
					<button id="js-btn-booking" class="btn btn--hollow contact__btn no-ajax is-active"><?php _e('Booking', 'lionlab'); ?></button>

					<button id="js-btn-message" class="btn btn--hollow contact__btn no-ajax"><?php _e('Kontakt', 'lionlab'); ?></button>
				</div>
				
				<div class="col-sm-8 col-sm-offset-2 contact__item">

					<div id="js-form-booking" class="contact__form is-active">
						<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
					</div>

					<div id="js-form-message" class="contact__form">
						<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
					</div>
				</div>

			</div>
		</div>
	</section>
</main>

<?php get_template_part('parts/footer'); ?>