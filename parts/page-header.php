<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}


	//bg
	$img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;
?>

<div class="page__hero--wrapper">
	<div class="page__hero" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
		
	</div>
</div>

<section class="page__intro">
	<div class="wrap hpad">
		<h1 class="page__title"><?php echo $title; ?></h1>
	</div>
</section>