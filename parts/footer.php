</div>
</div>

<?php 
	if (ICL_LANGUAGE_CODE === 'da') { 
		$locale = 'da-DK';
	} 

	elseif (ICL_LANGUAGE_CODE === 'en') { 
		$locale = 'en-US';
	} 

	elseif (ICL_LANGUAGE_CODE === 'it') {
		$locale = 'it-IT';
	}
?>

<!-- TrustBox widget - Micro Review Count -->
<div class="trustpilot">
	<div class="trustpilot-widget" data-locale="<?php echo esc_attr($locale); ?>" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5cca9a854786990001506c0e" data-style-height="54px" data-style-width="120px" data-theme="light">
	  <a href="https://dk.trustpilot.com/review/steptransport.dk" target="_blank" rel="noopener">Trustpilot</a>
	</div>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/step_trustscore.png" alt="">
</div>
<!-- End TrustBox widget -->

<?php get_template_part('parts/keyword', 'banner'); ?>

<footer class="footer gray-dark--bg" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<?php if ($title) : ?> 
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>

</body>
</html>
