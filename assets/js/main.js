$(document).ready(function() {

  //in viewport check
  function viewportCheck() {
    var $animation_elements = $('.anim');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');
  }

  viewportCheck();


  //menu toggle
  function menuToggle() {
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }

  menuToggle();

  //lazyload
  function lazyLoad() {
    var bLazy = new Blazy({ 
      offset: 0 //load images 0px before they're visible
    });
  }

  lazyLoad();

  //fancybox
  $('[data-fancybox]').fancybox({   
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    } 
  });


 //owl slider/carousel
 function OwlSlider() {
    var owl = $('.slider__track');

    owl.each(function() {
    $(this).children().length > 1;

      owl.owlCarousel({ 
          loop: true,
          items: 1,
          autoplay: true,
          mouseDrag: false,
          nav: true,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 2200,
          navSpeed: 2200,
          autoHeight:true,
          animateIn: 'fadeIn',
          animateOut: 'fadeOut',
          navText : [""] 
      });

    });
  }

  OwlSlider();


  //toggle contact forms
  function contactFormToggle() {
    $('#js-btn-booking').on('click', function() {
      $(this).addClass('is-active');
      $('#js-form-booking').addClass('is-active');
      $('#js-form-message').removeClass('is-active');
      $('#js-btn-message').removeClass('is-active');
    });

    $('#js-btn-message').on('click', function() {
      $(this).addClass('is-active');
      $('#js-form-booking').removeClass('is-active');
      $('#js-form-message').addClass('is-active');
      $('#js-btn-booking').removeClass('is-active');
    });
  }

  contactFormToggle();


  //toggle job description
  function jobToggle() {
    var $acc = $('.jobs__table-trigger');  

      $acc.each(function() {
        $(this).click(function() {
          $(this).toggleClass('is-active');
          $(this).next().toggleClass('show');
      });
    });
  }

  jobToggle();


  //fade in menu-items
  function fadeMenu() {
    $('.nav-toggle').click(function() {
      $('.nav__item').each(function() {        
            
        $(this).css({
          opacity: 1,
          transform: 'translateY(-1rem)',
          'transition' : 'all ease .6s',
          'transition-delay' : '.6s'
        });

      })
    }); 
  }

  fadeMenu();


  //disable ajax
  function disableAjax () {
    $('.wpml-ls-item a').addClass('no-ajax');
  }

  disableAjax();



  //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'transition';
  Barba.Pjax.Dom.containerClass = 'transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
      $('.page__hero').addClass('is-animated');  
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      $('.page__hero').removeClass('is-animated');
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.

    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
      $('.page__hero').removeClass('is-animated');
    }
  });


  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() {

  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $(window).scrollTop(0); // scroll to top here

    $newContainer.css({
      visibility : 'visible',
      opacity: 0
    });

    $newContainer.animate({
      opacity: 1,
    }, 400, function() {
      /**
      * Do not forget to call .done() as soon your transition is finished!
      * .done() will automatically remove from the DOM the old Container
      */
      _this.done();

    });
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return FadeTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    menuToggle();
    $('.nav__item').removeAttr('style');
    fadeMenu();
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');

    //send page view on every page transition
    if (Barba.HistoryManager.history.length <= 1) {
        return;
    }

    if (typeof gtag === 'function') {
        // send statics by Google Analytics(gtag.js)
        gtag('config', 'GTM-WV8MH5W', {'page_path': location.pathname, 'use_amp_client_id': true});
    } 

    else {
        // send statics by Google Analytics(analytics.js) or Google Tag Manager
        if (typeof ga === 'function') {
            var trackers = ga.getAll();
            trackers.forEach(function (tracker) {
                ga(tracker.get('name') + '.send', 'pageview', location.pathname, {'useAmpClientId': true});
            });
        }
    }

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );

    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);

    // get header element
    var $header = $('header');
    var $nav = $('nav');
    
    // change header class
    $header.removeClass();
    $nav.removeClass();
    $header.addClass(newPageRawHTML.match(/<header.+?class="([^""]*)"/i)[1]);
    $nav.addClass(newPageRawHTML.match(/<nav.+?class="([^""]*)"/i)[1]);

    fadeMenu();
    menuToggle();
    jobToggle();
    viewportCheck();
    lazyLoad();
    disableAjax(); 
    OwlSlider();

    if ($('body').hasClass('contact')) {
      contactFormToggle();
    }


    //get current url and path
    var $path = currentStatus.url.split(window.location.origin)[1].substring(1); 
    var $url = currentStatus.url;

    //add active class based upon URL status
    $('.nav__item a').each(function() {  
      if ($url === this.href) {
        $(this).addClass('is-active');
      }

      else {
        $(this).removeClass('is-active');
        $('.nav__item').removeClass('is-active');
      }
    });

  }); 

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {

  });

});